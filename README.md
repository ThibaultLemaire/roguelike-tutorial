# Doryen-rs Roguelike Example

A port of [the libtcod tutorial] game to [doryen-rs].

![screenshot](roguelike-doryen/static/screenshot.png)

I originally wrote this game following [this great tutorial] by [Tomas Sedovic], which in turn is a port of [the Python tutorial][the libtcod tutorial] for the [Rust] language.
I then wanted to compile it to [Webassembly] to host it on [Gitlab Pages], but couldn't because [libtcod] doesn't support the wasm target as of yet. So I ported it over to [doryen-rs]!

If you have followed either of the tutorials, you'll probably recognise the game, but not the code. I have had to make non-trivial changes to bend to the way [doryen-rs] is designed (and also some changes as a matter of personal taste).

Version 1.0.0 of the `roguelike-doryen` crate is a faithful port of the original game to the web. I wanted it to be as close as possible before starting to experiment with it. The following minor versions will probably be small tweaks to balance the difficulty.

The `roguelike-tcod` crate is the remainder of the tutorial code. I tried to keep it working while extracting its parts to the `roguelike-common` library.


[the libtcod tutorial]:http://www.roguebasin.com/index.php?title=Complete_Roguelike_Tutorial,_using_python%2Blibtcod
[this great tutorial]: https://tomassedovic.github.io/roguelike-tutorial/
[Tomas Sedovic]: https://github.com/tomassedovic
[Rust]: https://www.rust-lang.org/
[Webassembly]: https://webassembly.org/
[Gitlab Pages]: https://docs.gitlab.com/ee/user/project/pages/
[libtcod]: https://github.com/libtcod/libtcod
[doryen-rs]: https://github.com/jice-nospam/doryen-rs

## My Development Environment

I don't know what works for you, but the following works for me:

1. Install [Nix]. Nix is a functional package manager that simplifies the process of setting up a project environment.

1. Start a Nix shell. This will give you an environment with the required dependencies.

    ``` sh
    nix-shell
    ```

1. Install the appropriate toolchain.

    ``` sh
    rustup override set 1.39.0
    rustup target add wasm32-unknown-unknown
    ```

1. Install `cargo-web`. This cargo sub-command makes it easy to build wasm projects with Rust.

    ``` sh
    cargo install cargo-web
    ```

1. Build and run the project.

    ``` sh
    cargo web start -p roguelike-doryen
    ```

[Nix]: https://nixos.org/nix/
