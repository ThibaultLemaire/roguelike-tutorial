use roguelike_common::colors::*;
use roguelike_common::{
    ais_take_turn, cast_heal, cast_lightning, render_map, sort_visible_objects, toggle_equipment,
    Ai, FieldOfView, Fighter, Fighting, Item, Level, Map, Messages, Object, Player, Position,
    SaveState, UseResult, Vector2, BAR_WIDTH, CHARACTER_SCREEN_WIDTH, CONFUSE_NUM_TURNS,
    CONFUSE_RANGE, CONSOLE_HEIGHT, CONSOLE_WIDTH, DOWN, FIREBALL_DAMAGE, FIREBALL_RADIUS,
    GREETINGS, INVENTORY_WIDTH, LEFT, LEVEL_SCREEN_WIDTH, LIMIT_FPS, MAP_HEIGHT, MSG_HEIGHT, MSG_X,
    PANEL_HEIGHT, RIGHT, UP,
};
use std::cmp;
use std::error::Error;
use std::fs::File;
use tcod::colors::Color;
use tcod::console::*;
use tcod::input::{self, Event, Key, Mouse};
use tcod::map::FovAlgorithm;

const SCREEN_WIDTH: i32 = CONSOLE_WIDTH as i32;
const SCREEN_HEIGHT: i32 = CONSOLE_HEIGHT as i32;

const FOV_ALGO: FovAlgorithm = FovAlgorithm::Basic;

const PANEL_Y: i32 = MAP_HEIGHT as i32;

const MSG_WIDTH: i32 = SCREEN_WIDTH - BAR_WIDTH - 2;

trait ToColor {
    fn to_color(self) -> Color;
}
impl ToColor for RGB {
    fn to_color(self) -> Color {
        let Self { r, g, b } = self;
        Color { r, g, b }
    }
}

trait TcodObject {
    fn draw(&self, con: &mut dyn Console);
    fn heal(&mut self, amount: i32);
    fn max_hp(&self) -> i32;
}
impl TcodObject for Object {
    fn draw(&self, con: &mut dyn Console) {
        con.set_default_foreground(self.color.to_color());
        let Position { x, y } = self.pos;
        con.put_char(x as i32, y as i32, self.char, BackgroundFlag::None);
    }

    fn heal(&mut self, amount: i32) {
        let max_hp = self.max_hp();
        if let Some(Fighter { hp, .. }) = self.fighter.as_mut() {
            *hp += cmp::min(amount, max_hp - *hp);
        }
    }

    fn max_hp(&self) -> i32 {
        self.fighter.map_or(0, |f| f.base_max_hp)
    }
}

trait GetPosition {
    fn pos(self) -> Position;
}

impl GetPosition for Mouse {
    fn pos(self) -> Position {
        Position {
            x: self.cx as u32,
            y: self.cy as u32,
        }
    }
}

struct FovMap {
    map: tcod::map::Map,
    algo: FovAlgorithm,
    radius: i32,
    light_walls: bool,
}

impl FieldOfView for FovMap {
    fn new(width: usize, height: usize, radius: usize, light_walls: bool) -> Self {
        Self {
            map: tcod::map::Map::new(width as i32, height as i32),
            algo: FOV_ALGO,
            radius: radius as i32,
            light_walls,
        }
    }
    fn compute_fov(&mut self, &Position { x, y }: &Position) {
        self.map
            .compute_fov(x as i32, y as i32, self.radius, self.light_walls, self.algo)
    }
    fn is_in_fov(&self, x: usize, y: usize) -> bool {
        self.map.is_in_fov(x as i32, y as i32)
    }
    fn set_transparent(&mut self, x: usize, y: usize, transparent: bool) {
        self.map.set(x as i32, y as i32, transparent, false);
    }
}

struct Tcod {
    root: Root,
    con: Offscreen,
    panel: Offscreen,
    key: Key,
    mouse: Mouse,
}

struct Game {
    player: Player,
    messages: Messages,
    level: Level<FovMap>,
}

impl Game {
    fn new() -> Self {
        let mut player = Player {
            object: Object::player(),
            inventory: vec![Object::dagger()],
        };
        Self {
            level: Level::new(&mut player.object.pos),
            player,
            messages: Messages::new(GREETINGS, RED),
        }
    }
}

#[derive(Clone, Copy, Debug)]
enum PlayerAction {
    TookTurn,
    DidntTakeTurn,
    Exit,
}

pub fn move_by(id: usize, delta: Vector2, map: &Map, objects: &mut [Object]) {
    let mut object = objects[id].clone();
    object.move_by(delta, map, objects.iter());
    objects[id] = object;
}

fn handle_keys(tcod: &mut Tcod, game: &mut Game) -> PlayerAction {
    use tcod::input::KeyCode::*;
    use PlayerAction::*;

    let player_alive = game.player.object.alive;
    let mut move_player = |direction| {
        game.player.move_or_attack(
            direction,
            &mut game.level.objects,
            &game.level.map,
            &mut game.messages,
        );
        TookTurn
    };

    match (tcod.key, tcod.key.text(), player_alive) {
        (Key { code: Escape, .. }, _, _) => Exit, // exit game
        (
            Key {
                code: Enter,
                alt: true,
                ..
            },
            _,
            _,
        ) => {
            let fullscreen = tcod.root.is_fullscreen();
            tcod.root.set_fullscreen(!fullscreen);
            DidntTakeTurn
        }
        (Key { code: Up, .. }, _, true) => move_player(UP),
        (Key { code: Down, .. }, _, true) => move_player(DOWN),
        (Key { code: Left, .. }, _, true) => move_player(LEFT),
        (Key { code: Right, .. }, _, true) => move_player(RIGHT),
        (Key { code: Spacebar, .. }, _, true) => TookTurn,
        (Key { code: Text, .. }, "g", true) => {
            game.player
                .pick_item_up(&mut game.level.objects, &mut game.messages);
            DidntTakeTurn
        }
        (Key { code: Text, .. }, "i", true) => {
            let inventory_index = inventory_menu(
                &game.player.inventory,
                "Press the key next to an item to use it, or any other to cancel.\n",
                &mut tcod.root,
            );
            if let Some(inventory_index) = inventory_index {
                use_item(inventory_index, tcod, game);
            }
            DidntTakeTurn
        }
        (Key { code: Text, .. }, "d", true) => {
            let inventory_index = inventory_menu(
                &game.player.inventory,
                "Press the key next to an item to drop it, or any other to cancel.\n",
                &mut tcod.root,
            );
            if let Some(inventory_index) = inventory_index {
                game.player
                    .drop_item(inventory_index, &mut game.messages, &mut game.level.objects);
            }
            DidntTakeTurn
        }
        (Key { code: Text, .. }, "c", true) => {
            msgbox(
                &game.player.stats(),
                CHARACTER_SCREEN_WIDTH as i32,
                &mut tcod.root,
            );
            DidntTakeTurn
        }
        (Key { code: Text, .. }, "<", true) => {
            game.level
                .take_stairs(&mut game.player.object, &mut game.messages);
            DidntTakeTurn
        }
        _ => DidntTakeTurn,
    }
}

fn get_names_under_mouse(mouse: Mouse, objects: &[Object], fov_map: &FovMap) -> String {
    let names = objects
        .iter()
        .filter(|obj| {
            obj.pos == mouse.pos() && fov_map.is_in_fov(obj.pos.x as usize, obj.pos.y as usize)
        })
        .map(|obj| obj.name.clone())
        .collect::<Vec<_>>();

    names.join(", ")
}

fn render_all(tcod: &mut Tcod, game: &mut Game, fov_recompute: bool) {
    if fov_recompute {
        game.level.fov.compute_fov(&game.player.object.pos);
    }
    let con = &mut tcod.con;
    render_map(&mut game.level.map, &game.level.fov, |x, y, rgb| {
        con.set_char_background(x as i32, y as i32, rgb.to_color(), BackgroundFlag::Set)
    });

    for object in sort_visible_objects(&game.level.objects, &game.level.map, &game.level.fov) {
        object.draw(&mut tcod.con);
    }
    game.player.object.draw(&mut tcod.con);

    blit(
        &tcod.con,
        (0, 0),
        (SCREEN_WIDTH, MAP_HEIGHT as i32),
        &mut tcod.root,
        (0, 0),
        1.0,
        1.0,
    );

    tcod.panel.set_default_background(BLACK.to_color());
    tcod.panel.set_default_foreground(WHITE.to_color());
    tcod.panel.clear();

    let player = &game.player.object;
    let max_hp = player.max_hp();
    let hp = player.fighter.map_or(0, |f| f.hp);
    render_bar(
        &mut tcod.panel,
        1,
        1,
        BAR_WIDTH,
        "HP",
        hp,
        max_hp,
        LIGHT_RED,
        DARKER_RED,
    );

    tcod.panel.print_ex(
        1,
        3,
        BackgroundFlag::None,
        TextAlignment::Left,
        game.level.panel_text(),
    );

    tcod.panel.set_default_foreground(LIGHT_GREY.to_color());
    tcod.panel.print_ex(
        1,
        0,
        BackgroundFlag::None,
        TextAlignment::Left,
        get_names_under_mouse(tcod.mouse, &game.level.objects, &game.level.fov),
    );

    let mut y = MSG_HEIGHT as i32;
    for (msg, rgb) in game.messages.iter().rev() {
        let msg_height = tcod.panel.get_height_rect(MSG_X, y, MSG_WIDTH, 0, msg);
        y -= msg_height;
        if y < 0 {
            break;
        }
        tcod.panel.set_default_foreground(rgb.to_color());
        tcod.panel.print_rect(MSG_X, y, MSG_WIDTH, 0, msg);
    }

    blit(
        &tcod.panel,
        (0, 0),
        (SCREEN_WIDTH, PANEL_HEIGHT as i32),
        &mut tcod.root,
        (0, PANEL_Y),
        1.0,
        1.0,
    );
}

fn render_bar<C: ToColor>(
    panel: &mut Offscreen,
    x: i32,
    y: i32,
    total_width: i32,
    name: &str,
    value: i32,
    maximum: i32,
    bar_color: C,
    back_color: C,
) {
    let bar_width = (value as f32 / maximum as f32 * total_width as f32) as i32;

    panel.set_default_background(back_color.to_color());
    panel.rect(x, y, total_width, 1, false, BackgroundFlag::Screen);

    panel.set_default_background(bar_color.to_color());
    if bar_width > 0 {
        panel.rect(x, y, bar_width, 1, false, BackgroundFlag::Screen);
    }

    panel.set_default_background(WHITE.to_color());
    panel.print_ex(
        x + total_width / 2,
        y,
        BackgroundFlag::None,
        TextAlignment::Center,
        &format!("{}: {}/{}", name, value, maximum),
    );
}

fn menu<T: AsRef<str>>(header: &str, options: &[T], width: i32, root: &mut Root) -> Option<usize> {
    assert!(
        options.len() <= 26,
        "Cannot have a menu with more than 26 options."
    );
    let header_height = if header.is_empty() {
        0
    } else {
        root.get_height_rect(0, 0, width, SCREEN_HEIGHT, header)
    };
    let height = options.len() as i32 + header_height;

    let mut window = Offscreen::new(width, height);

    window.set_default_foreground(WHITE.to_color());
    window.print_rect_ex(
        0,
        0,
        width,
        height,
        BackgroundFlag::None,
        TextAlignment::Left,
        header,
    );

    for (index, option_text) in options.iter().enumerate() {
        let menu_letter = (b'a' + index as u8) as char;
        let text = format!("({}) {}", menu_letter, option_text.as_ref());
        window.print_ex(
            0,
            header_height + index as i32,
            BackgroundFlag::None,
            TextAlignment::Left,
            text,
        );
    }

    let x = SCREEN_WIDTH / 2 - width / 2;
    let y = SCREEN_HEIGHT / 2 - height / 2;
    blit(&window, (0, 0), (width, height), root, (x, y), 1.0, 0.7);

    root.flush();
    let key = root.wait_for_keypress(true).printable;
    if key.is_alphabetic() {
        let index = key.to_ascii_lowercase() as usize - 'a' as usize;
        if index < options.len() {
            return Some(index);
        }
    }

    None
}

fn inventory_menu(inventory: &[Object], header: &str, root: &mut Root) -> Option<usize> {
    let options = if inventory.len() == 0 {
        vec!["Inventory is empty.".into()]
    } else {
        inventory
            .iter()
            .map(|item| match &item.equipment {
                Some(equipment) if equipment.equipped => {
                    format!("{} (on {})", item.name, equipment.slot)
                }
                _ => item.name.clone(),
            })
            .collect()
    };

    let inventory_index = menu(header, &options, INVENTORY_WIDTH as i32, root);

    if inventory.len() > 0 {
        inventory_index
    } else {
        None
    }
}

fn confuse_effect(monster_id: usize, game: &mut Game) {
    let old_ai = game.level.objects[monster_id]
        .ai
        .take()
        .expect("Tried to target non-ai.");

    game.level.objects[monster_id].ai = Some(Ai::Confused {
        previous_ai: Box::new(old_ai),
        num_turns: CONFUSE_NUM_TURNS,
    });
    game.messages.add(
        format!(
            "The eyes of {} look vacant, as he starts to stumble around!",
            game.level.objects[monster_id].name
        ),
        LIGHT_GREEN,
    );
}

fn cast_confuse(_inventory_id: usize, tcod: &mut Tcod, game: &mut Game) -> UseResult {
    game.messages.add(
        "Left-click an enemy to confuse it, or right-click to cancel.",
        LIGHT_CYAN,
    );
    let monster_id = target_monster(tcod, game, Some(CONFUSE_RANGE));
    match monster_id {
        Some(monster_id) => {
            confuse_effect(monster_id, game);
            UseResult::UsedUp
        }
        None => UseResult::Cancelled,
    }
}

fn cast_fireball(_inventory_id: usize, tcod: &mut Tcod, game: &mut Game) -> UseResult {
    // fixme: should damage player
    game.messages.add(
        "Left-click a target tile for the fireball, or right-click to cancel.",
        LIGHT_CYAN,
    );
    let pos = match target_tile(tcod, game, None) {
        Some(tile_pos) => tile_pos,
        None => return UseResult::Cancelled,
    };
    game.messages.add(
        format!(
            "The fireball explodes, burning everything within {} tiles!",
            FIREBALL_RADIUS
        ),
        ORANGE,
    );

    let mut xp_to_gain = 0;
    for obj in game.level.objects.iter_mut() {
        if obj.pos.sqr_distance(&pos) <= FIREBALL_RADIUS.pow(2) && obj.fighter.is_some() {
            game.messages.add(
                format!(
                    "The {} gets burned for {} hit points.",
                    obj.name, FIREBALL_DAMAGE
                ),
                ORANGE,
            );
            if let Some(xp) = obj.take_damage(FIREBALL_DAMAGE, &mut game.messages) {
                xp_to_gain += xp;
            }
        }
    }
    game.player.object.fighter.as_mut().unwrap().xp += xp_to_gain;

    UseResult::UsedUp
}

fn use_item(inventory_id: usize, tcod: &mut Tcod, game: &mut Game) {
    use Item::*;
    use UseResult::*;
    enum Effect {
        New(fn(usize, &mut Player, &mut Messages, &mut [Object], &FovMap) -> UseResult),
        Old(fn(usize, &mut Tcod, &mut Game) -> UseResult),
    }
    use Effect::*;

    let effect = game.player.inventory[inventory_id]
        .item
        .map(|item| match item {
            Heal => New(cast_heal),
            Lightning => New(cast_lightning),
            Equipment => New(toggle_equipment),
            Confuse => Old(cast_confuse),
            Fireball => Old(cast_fireball),
        });
    let result = match effect {
        None => {
            game.messages.add(
                format!(
                    "The {} cannot be used",
                    game.player.inventory[inventory_id].name
                ),
                WHITE,
            );
            return;
        }
        Some(New(effect)) => effect(
            inventory_id,
            &mut game.player,
            &mut game.messages,
            &mut game.level.objects,
            &game.level.fov,
        ),
        Some(Old(effect)) => effect(inventory_id, tcod, game),
    };
    match result {
        UsedUp => {
            game.player.inventory.swap_remove(inventory_id);
        }
        Cancelled => {
            game.messages.add("Cancelled", WHITE);
        }
        UsedAndKept => {}
    }
}

fn target_tile(tcod: &mut Tcod, game: &mut Game, max_range: Option<u32>) -> Option<Position> {
    use tcod::input::KeyCode::Escape;

    loop {
        tcod.root.flush();
        let event = input::check_for_event(input::KEY_PRESS | input::MOUSE).map(|(_, e)| e);

        match event {
            Some(Event::Mouse(m)) => tcod.mouse = m,
            Some(Event::Key(k)) => tcod.key = k,
            None => tcod.key = Default::default(),
        }
        render_all(tcod, game, false);

        let pos = tcod.mouse.pos();

        let in_fov = (pos.x < CONSOLE_WIDTH as u32)
            && (pos.y < MAP_HEIGHT as u32)
            && game.level.fov.is_in_fov(pos.x as usize, pos.y as usize);
        let in_range = max_range.map_or(true, |range| {
            game.player.object.pos.sqr_distance(&pos) <= range.pow(2)
        });
        if tcod.mouse.lbutton_pressed && in_fov && in_range {
            return Some(pos);
        }
        if tcod.mouse.rbutton_pressed || tcod.key.code == Escape {
            return None;
        }
    }
}

fn target_monster(tcod: &mut Tcod, game: &mut Game, max_range: Option<u32>) -> Option<usize> {
    loop {
        match target_tile(tcod, game, max_range) {
            Some(pos) => {
                for (id, obj) in game.level.objects.iter().enumerate() {
                    if obj.pos == pos && obj.fighter.is_some() {
                        return Some(id);
                    }
                }
            }
            None => return None,
        }
    }
}

fn level_up(tcod: &mut Tcod, game: &mut Game, player: &mut Object) -> Option<()> {
    let level_up_xp = player.level_up_xp();
    let Fighter {
        xp,
        hp,
        base_max_hp,
        base_power,
        base_defense,
        ..
    } = player.fighter.as_mut()?;

    if level_up_xp <= *xp {
        player.level += 1;
        *xp -= level_up_xp;

        game.messages.add(
            format!(
                "Your battle skills grow stronger! You reached level {}!",
                player.level
            ),
            YELLOW,
        );

        let mut choice = None;
        while choice.is_none() {
            choice = menu(
                "Level up! Choose a stat to raise:\n",
                &[
                    format!("Constitution (+20 HP, from {})", base_max_hp),
                    format!("Strength (+1 attack, from {})", base_power),
                    format!("Agility (+1 defense, from {})", base_defense),
                ],
                LEVEL_SCREEN_WIDTH as i32,
                &mut tcod.root,
            );
        }
        match choice.unwrap() {
            0 => {
                *base_max_hp += 20;
                *hp += 20;
            }
            1 => *base_power += 1,
            2 => *base_defense += 1,
            _ => unreachable!(),
        }
    }

    Some(())
}

fn play_game(tcod: &mut Tcod, mut game: Game) {
    let mut previous_player_position = &game.player.object.pos + Vector2::right();

    while !tcod.root.window_closed() {
        tcod.con.set_default_foreground(WHITE.to_color());
        tcod.con.clear();
        let fov_recompute = previous_player_position != game.player.object.pos;

        match input::check_for_event(input::MOUSE | input::KEY_PRESS) {
            Some((_, Event::Mouse(m))) => tcod.mouse = m,
            Some((_, Event::Key(k))) => tcod.key = k,
            _ => tcod.key = Default::default(),
        }

        render_all(tcod, &mut game, fov_recompute);
        tcod.root.flush();

        let mut player = game.player.object.clone();
        level_up(tcod, &mut game, &mut player);
        game.player.object = player;

        previous_player_position = game.player.object.pos.clone();
        let player_action = handle_keys(tcod, &mut game);

        match (player_action, game.player.object.alive) {
            (PlayerAction::Exit, _) => {
                let Game {
                    player,
                    messages,
                    level,
                } = game;
                let state = SaveState::new(player, messages, level);
                save_game(&state).expect("Failed saving game");
                break;
            }
            (PlayerAction::TookTurn, true) => {
                ais_take_turn(
                    &mut game.level.objects,
                    &mut game.player,
                    &mut game.messages,
                    &game.level.fov,
                    &game.level.map,
                );
            }
            _ => {}
        }
    }
}

fn msgbox(text: &str, width: i32, root: &mut Root) {
    let options: &[&str] = &[];
    menu(text, options, width, root);
}

fn main_menu(tcod: &mut Tcod) {
    let img = tcod::image::Image::from_file("menu_background.png")
        .ok()
        .expect("Background image not found");

    while !tcod.root.window_closed() {
        tcod::image::blit_2x(&img, (0, 0), (-1, -1), &mut tcod.root, (0, 0));

        tcod.root.set_default_foreground(LIGHT_YELLOW.to_color());
        tcod.root.print_ex(
            SCREEN_WIDTH / 2,
            SCREEN_HEIGHT / 2 - 4,
            BackgroundFlag::None,
            TextAlignment::Center,
            "TOMBS OF THE ANCIENT KINGS",
        );
        tcod.root.print_ex(
            SCREEN_WIDTH / 2,
            SCREEN_HEIGHT - 2,
            BackgroundFlag::None,
            TextAlignment::Center,
            "By Thibault Lemaire, on the instructions of Tomas Sedovic",
        );

        let choices = &["Play a new game", "Continue last game", "Quit"];
        let choice = menu("", choices, 24, &mut tcod.root);

        match choice {
            Some(0) => {
                play_game(tcod, Game::new());
            }
            Some(1) => match load_game() {
                Ok(state) => {
                    let (player, messages, level) = state.open();
                    let game = Game {
                        player,
                        messages,
                        level,
                    };
                    play_game(tcod, game);
                }
                Err(_e) => msgbox("\nNo saved game to load.\n", 24, &mut tcod.root),
            },
            Some(2) => break,
            _ => {}
        }
    }
}

fn save_game(state: &SaveState) -> Result<(), Box<dyn Error>> {
    let file = File::create("savegame")?;
    serde_json::to_writer(file, state)?;
    Ok(())
}

fn load_game() -> Result<SaveState, Box<dyn Error>> {
    let file = File::open("savegame")?;
    let state = serde_json::from_reader(file)?;
    Ok(state)
}

fn main() {
    tcod::system::set_fps(LIMIT_FPS as i32);

    let root = Root::initializer()
        .font("arial10x10.png", FontLayout::Tcod)
        .font_type(FontType::Greyscale)
        .size(SCREEN_WIDTH, SCREEN_HEIGHT)
        .title("Rust/libtcod tutorial")
        .init();

    let mut tcod = Tcod {
        root,
        con: Offscreen::new(SCREEN_WIDTH, MAP_HEIGHT as i32),
        panel: Offscreen::new(SCREEN_WIDTH, PANEL_HEIGHT as i32),
        key: Default::default(),
        mouse: Default::default(),
    };

    main_menu(&mut tcod);
}
