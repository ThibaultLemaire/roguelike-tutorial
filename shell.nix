{ pkgs ? import <nixpkgs> {} }:

with pkgs;
mkShell {
  buildInputs = [
    rustup
    pkg-config
    SDL2
  ];
}
