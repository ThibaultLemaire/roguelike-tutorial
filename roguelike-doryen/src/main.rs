use doryen_fov::{FovAlgorithm, FovRestrictive, MapData};
use doryen_rs::{
    App, AppOptions, Console, DoryenApi, Engine, Image, InputApi, TextAlign, UpdateEvent,
};
use roguelike_common::colors::{
    BLACK, DARKER_RED, LIGHT_CYAN, LIGHT_GREEN, LIGHT_GREY, LIGHT_RED, LIGHT_YELLOW, ORANGE, RED,
    WHITE, YELLOW,
};
use roguelike_common::{
    ais_take_turn, cast_heal, cast_lightning, render_map, sort_visible_objects, toggle_equipment,
    visible_objects, Ai, FieldOfView, Fighter, Fighting, Item, Level, Map, Messages, Object,
    Player, Position, SaveState, UseResult, BAR_WIDTH, CHARACTER_SCREEN_WIDTH, CONFUSE_NUM_TURNS,
    CONFUSE_RANGE, CONSOLE_HEIGHT, CONSOLE_WIDTH, DOWN, FIREBALL_DAMAGE, FIREBALL_RADIUS,
    GREETINGS, IDLE, INVENTORY_WIDTH, LEFT, LEVEL_SCREEN_WIDTH, LIMIT_FPS, MAP_HEIGHT, MSG_HEIGHT,
    MSG_X, PANEL_HEIGHT, RIGHT, UP,
};
use ron::de::{from_str as read_ron, Error as RonDeError};
use ron::ser::to_string as write_ron;
use std::iter::once;
use textwrap::wrap_iter;

const MOVE_COOLDOWN_TICKS: u8 = 5;

trait MapConsole {
    fn clear_chars(&mut self);
    fn render_map(&mut self, map: &mut Map, fov: &impl FieldOfView);
    fn render_object(&mut self, object: &Object);
    fn render_objects(&mut self, objects: &[Object], map: &Map, fov: &impl FieldOfView);
}

impl MapConsole for Console {
    fn clear_chars(&mut self) {
        self.clear(None, Some(BLACK.into()), Some(' ' as u16));
    }

    fn render_map(&mut self, map: &mut Map, fov: &impl FieldOfView) {
        render_map(map, fov, |x, y, rgb| {
            self.back(x as i32, y as i32, rgb.into())
        });
    }

    fn render_object(
        &mut self,
        &Object {
            pos: Position { x, y },
            char,
            color,
            ..
        }: &Object,
    ) {
        self.cell(
            x as i32,
            y as i32,
            Some(char as u16),
            Some(color.into()),
            None,
        );
    }

    fn render_objects(&mut self, objects: &[Object], map: &Map, fov: &impl FieldOfView) {
        for object in sort_visible_objects(objects, map, fov) {
            self.render_object(object)
        }
    }
}

struct FoV {
    fov: FovRestrictive,
    map: MapData,
    radius: usize,
    light_walls: bool,
}

impl FieldOfView for FoV {
    fn new(width: usize, height: usize, radius: usize, light_walls: bool) -> Self {
        Self {
            fov: FovRestrictive::new(),
            map: MapData::new(width, height),
            radius,
            light_walls,
        }
    }
    fn compute_fov(&mut self, &Position { x, y }: &Position) {
        self.map.clear_fov();
        self.fov.compute_fov(
            &mut self.map,
            x as usize,
            y as usize,
            self.radius,
            self.light_walls,
        )
    }
    fn is_in_fov(&self, x: usize, y: usize) -> bool {
        self.map.is_in_fov(x, y)
    }
    fn set_transparent(&mut self, x: usize, y: usize, transparent: bool) {
        self.map.set_transparent(x, y, transparent);
    }
}

struct MenuConsole(Console);

impl MenuConsole {
    fn new(header: &str, options: &[impl AsRef<str>], width: usize) -> (Self, usize) {
        let mut options_lines: Vec<String> = options
            .iter()
            .enumerate()
            .map(|(index, option_text)| {
                let menu_letter = (b'a' + index as u8) as char;
                format!("({}) {}", menu_letter, option_text.as_ref())
            })
            .collect();
        let option_count = options_lines.len();
        assert!(
            option_count <= 26,
            "Cannot have a menu with more than 26 options."
        );
        let mut header_lines: Vec<String> = wrap_iter(header, width).map(String::from).collect();
        header_lines.append(&mut options_lines);
        let lines = header_lines;
        let white = Some(WHITE.into());
        let width = width as u32;
        let height = lines.len() as u32;
        let mut con = Console::new(width, height);
        for (i, line) in lines.iter().enumerate() {
            con.print(0, i as i32, line, TextAlign::Left, white, None);
        }

        (Self(con), option_count)
    }

    fn blit_centered(&mut self, root: &mut Console) {
        let x = root.get_width() / 2 - self.0.get_width() / 2;
        let y = root.get_height() / 2 - self.0.get_height() / 2;
        self.0.blit(x as i32, y as i32, root, 1.0, 0.7, None);
    }
}

struct Menu<T: Screen> {
    previous: Box<T>,
    callback: fn(Box<T>, Option<u8>) -> Box<dyn Screen>,
    option_count: u8,
    con: Option<MenuConsole>,
}

impl<T: Screen> Menu<T> {
    fn new(
        header: &str,
        options: &[impl AsRef<str>],
        width: usize,
        callback: fn(Box<T>, Option<u8>) -> Box<dyn Screen>,
    ) -> impl FnOnce(Box<T>) -> Self {
        let (con, count) = MenuConsole::new(header, options, width);
        move |previous| Self {
            previous,
            callback,
            con: Some(con),
            option_count: count as u8,
        }
    }

    fn inventory(
        header: &str,
        inventory: &[Object],
        callback: fn(Box<T>, Option<u8>) -> Box<dyn Screen>,
    ) -> impl FnOnce(Box<T>) -> Self {
        let header = if 0 < inventory.len() {
            header
        } else {
            "Inventory is empty."
        };
        let options: Vec<_> = inventory
            .iter()
            .map(|item| match &item.equipment {
                Some(equipment) if equipment.equipped => {
                    format!("{} (on {})", item.name, equipment.slot)
                }
                _ => item.name.clone(),
            })
            .collect();
        Self::new(header, &options, INVENTORY_WIDTH, callback)
    }

    fn call_back(self, choice: Option<u8>) -> Box<dyn Screen> {
        (self.callback)(self.previous, choice)
    }
}

impl<T: Screen + 'static> Screen for Menu<T> {
    fn update(self: Box<Self>, api: &mut dyn DoryenApi) -> Box<dyn Screen> {
        let input = api.input();
        if input.key("Escape") {
            return self.call_back(None);
        }

        let text = input.text();
        if text == "" {
            return self;
        }

        let choice = text
            .into_bytes()
            .iter()
            .map(|&chr| chr - b'a')
            .find(|&index| index < self.option_count);
        self.call_back(choice)
    }

    fn render(&mut self, api: &mut dyn DoryenApi) {
        if let Some(mut con) = self.con.take() {
            self.previous.render(api);
            con.blit_centered(api.con());
        }
    }
}

fn msgbox(text: &str, width: usize, previous: Box<impl Screen + 'static>) -> Menu<impl Screen> {
    Menu::new(text, &[] as &[&str], width, |previous, _| {
        previous as Box<dyn Screen>
    })(previous)
}

struct Game {
    player: Player,
    move_cooldown_ticks: u8,
    level: Level<FoV>,
    messages: Messages,
    panel: Console,
}

impl Game {
    fn new() -> Self {
        let mut player = Player {
            object: Object::player(),
            inventory: vec![Object::dagger()],
        };
        Self {
            level: Level::new(&mut player.object.pos),
            player,
            move_cooldown_ticks: 0,
            messages: Messages::new(GREETINGS, RED),
            panel: Console::new(CONSOLE_WIDTH as u32, PANEL_HEIGHT as u32),
        }
    }

    fn from(save: SaveState) -> Self {
        let (player, messages, level) = save.open();
        Self {
            level,
            player,
            messages,
            move_cooldown_ticks: 0,
            panel: Console::new(CONSOLE_WIDTH as u32, PANEL_HEIGHT as u32),
        }
    }

    fn render_health_bar(&mut self) {
        let (x, y, h) = (1, 1, 1);
        let (hp, max_hp) = self.player.object.fighter.map_or(
            (0, 0),
            |Fighter {
                 hp, base_max_hp, ..
             }| (hp, base_max_hp),
        );
        self.panel.area(
            x,
            y,
            BAR_WIDTH as u32,
            h,
            None,
            Some(DARKER_RED.into()),
            None,
        );
        let bar_width = (hp as f32 / max_hp as f32 * BAR_WIDTH as f32) as i32;
        if 0 < bar_width {
            self.panel.area(
                x,
                y,
                bar_width as u32,
                h,
                None,
                Some(LIGHT_RED.into()),
                None,
            );
        };
        self.panel.print(
            x + BAR_WIDTH / 2,
            y,
            &format!("HP: {}/{}", hp, max_hp),
            TextAlign::Center,
            Some(WHITE.into()),
            None,
        );
    }

    fn render_dungeon_level(&mut self) {
        self.panel.print(
            1,
            3,
            &self.level.panel_text(),
            TextAlign::Left,
            Some(WHITE.into()),
            None,
        );
    }

    fn objects_under_mouse<'a>(
        &'a mut self,
        (x, y): (f32, f32),
    ) -> impl Iterator<Item = &'a Object> {
        let visible_objects =
            visible_objects(&self.level.objects, &self.level.map, &self.level.fov);
        let pos = Position {
            x: x as u32,
            y: y as u32,
        };
        visible_objects
            .chain(once(&self.player.object))
            .filter(move |obj| obj.pos == pos)
    }

    fn render_names_under_mouse(&mut self, pos: (f32, f32)) {
        let names_under_mouse = self
            .objects_under_mouse(pos)
            .map(|obj| obj.name.as_ref())
            .collect::<Vec<&str>>()
            .join(", ");
        self.panel.print(
            1,
            0,
            &names_under_mouse,
            TextAlign::Left,
            Some(LIGHT_GREY.into()),
            None,
        )
    }

    fn render_panel(&mut self, api: &mut dyn DoryenApi) {
        self.panel.clear_chars();
        self.render_health_bar();
        self.render_dungeon_level();
        self.render_names_under_mouse(api.input().mouse_pos());
        for (i, (ref line, &rgb)) in self
            .messages
            .iter()
            .rev()
            .flat_map(|(msg, rgb)| {
                let mut lines: Vec<_> = wrap_iter(msg, CONSOLE_WIDTH as usize - MSG_X as usize)
                    .map(move |line| (line, rgb))
                    .collect();
                lines.reverse();
                lines
            })
            .take(MSG_HEIGHT as usize)
            .enumerate()
        {
            self.panel.print(
                MSG_X,
                MSG_HEIGHT - i as i32,
                line,
                TextAlign::Left,
                Some(rgb.into()),
                None,
            )
        }
        self.panel
            .blit(0, MAP_HEIGHT as i32, api.con(), 1.0, 1.0, None)
    }

    fn _take_turn(&mut self, input: &mut dyn InputApi) -> bool {
        if input.key("Space") {
            return true;
        }

        let mut direction = IDLE;
        if input.key("ArrowLeft") {
            direction += LEFT;
        }
        if input.key("ArrowRight") {
            direction += RIGHT;
        }
        if input.key("ArrowUp") {
            direction += UP;
        }
        if input.key("ArrowDown") {
            direction += DOWN;
        }
        if direction == IDLE {
            return false;
        };

        self.player.move_or_attack(
            direction,
            &mut self.level.objects,
            &self.level.map,
            &mut self.messages,
        );
        self.level.fov.compute_fov(&self.player.object.pos);
        true
    }

    fn take_turn(&mut self, input: &mut dyn InputApi) -> bool {
        if 0 < self.move_cooldown_ticks {
            self.move_cooldown_ticks -= 1;
            return false;
        }

        let took_turn = self._take_turn(input);
        if took_turn {
            self.move_cooldown_ticks = MOVE_COOLDOWN_TICKS;
        }
        took_turn
    }

    fn level_up(&mut self) -> Option<impl FnOnce(Box<Self>) -> Menu<Self>> {
        let player = &self.player.object;
        let level_up_xp = player.level_up_xp();
        let Fighter {
            xp,
            base_max_hp,
            base_power,
            base_defense,
            ..
        } = player.fighter?;

        if xp < level_up_xp {
            return None;
        }

        Some(Menu::new(
            "Level up! Choose a stat to raise:\n",
            &[
                format!("Constitution (+20 HP, from {})", base_max_hp),
                format!("Strength (+1 attack, from {})", base_power),
                format!("Agility (+1 defense, from {})", base_defense),
            ],
            LEVEL_SCREEN_WIDTH,
            |mut game: Box<Self>, choice| {
                if let Some(choice) = choice {
                    let player = &mut game.player.object;
                    let level_up_xp = player.level_up_xp();
                    player.level += 1;
                    game.messages.add(
                        format!(
                            "Your battle skills grow stronger! You reached level {}!",
                            player.level
                        ),
                        YELLOW,
                    );
                    let Fighter {
                        xp,
                        base_max_hp,
                        hp,
                        base_power,
                        base_defense,
                        ..
                    } = player.fighter.as_mut().unwrap();
                    *xp -= level_up_xp;
                    match choice {
                        0 => {
                            *base_max_hp += 20;
                            *hp += 20;
                        }
                        1 => *base_power += 1,
                        2 => *base_defense += 1,
                        _ => unreachable!(),
                    }
                }
                game as Box<dyn Screen>
            },
        ))
    }

    fn inventory(self: Box<Self>) -> Box<dyn Screen> {
        Box::new(Menu::inventory(
            "Press the key next to an item to use it, or any other to cancel.",
            &self.player.inventory,
            |mut game: Box<Self>, choice: Option<u8>| {
                if let Some(index) = choice.map(|i| i as usize) {
                    use Item::*;
                    use UseResult::*;

                    let dispatch = match game.player.inventory[index]
                        .item
                        .expect("non-item object found in inventory")
                    {
                        Heal => cast_heal,
                        Lightning => cast_lightning,
                        Equipment => toggle_equipment,
                        Confuse => {
                            game.messages.add(
                                "Left-click an enemy to confuse it, or hit Escape to cancel.",
                                LIGHT_CYAN,
                            );
                            return Box::new(MouseTargeting::confusion(game, index));
                        }
                        Fireball => {
                            game.messages.add(
                                "Left-click a target tile for the fireball, or hit Escape to cancel.",
                                LIGHT_CYAN,
                            );
                            return Box::new(MouseTargeting::fireball(game, index));
                        }
                    };
                    let result = dispatch(
                        index,
                        &mut game.player,
                        &mut game.messages,
                        &mut game.level.objects,
                        &game.level.fov,
                    );
                    match result {
                        UsedUp => return Box::new(PostEffect::used_up(game, index)),
                        Cancelled => return Box::new(PostEffect::cancelled(game, index)),
                        UsedAndKept => {}
                    }
                }
                game as Box<dyn Screen>
            },
        )(self))
    }

    fn save(self) {
        let _ = stdweb::web::window().local_storage().insert(
            "savegame",
            &write_ron(&SaveState::new(self.player, self.messages, self.level)).unwrap_or_default(),
        );
    }
}

trait Screen {
    fn update(self: Box<Self>, api: &mut dyn DoryenApi) -> Box<dyn Screen>;
    fn render(&mut self, api: &mut dyn DoryenApi);
}

impl Screen for Game {
    fn update(mut self: Box<Self>, api: &mut dyn DoryenApi) -> Box<dyn Screen> {
        let input = api.input();
        if input.key_pressed("Escape") {
            self.save();
            return Box::new(MainMenu::new());
        }

        if !self.player.is_alive() {
            return self;
        }

        if let Some(menu_factory) = self.level_up() {
            return Box::new(menu_factory(self));
        }

        let text = input.text();
        if text == "i" {
            return self.inventory();
        };

        if text == "d" {
            return Box::new(Menu::inventory(
                "Press the key next to an item to drop it, or any other to cancel.",
                &self.player.inventory,
                |mut game: Box<Self>, choice| {
                    if let Some(index) = choice {
                        game.player.drop_item(
                            index as usize,
                            &mut game.messages,
                            &mut game.level.objects,
                        )
                    }
                    game as Box<dyn Screen>
                },
            )(self));
        };

        if text == "c" {
            return Box::new(msgbox(&self.player.stats(), CHARACTER_SCREEN_WIDTH, self));
        }

        if text == "<" {
            let took_stairs = self
                .level
                .take_stairs(&mut self.player.object, &mut self.messages);
            if took_stairs {
                api.con().clear(None, Some(BLACK.into()), None);
                return self;
            }
        }

        if text == "g" {
            self.player
                .pick_item_up(&mut self.level.objects, &mut self.messages);
        }

        let took_turn = self.take_turn(input);
        if took_turn {
            ais_take_turn(
                &mut self.level.objects,
                &mut self.player,
                &mut self.messages,
                &self.level.fov,
                &self.level.map,
            );
        };
        self
    }

    fn render(&mut self, api: &mut dyn DoryenApi) {
        let root = api.con();
        root.clear_chars();
        root.render_map(&mut self.level.map, &self.level.fov);
        root.render_objects(&self.level.objects, &self.level.map, &self.level.fov);
        root.render_object(&self.player.object);
        self.render_panel(api)
    }
}

struct PostEffect {
    game: Box<Game>,
    item_index: usize,
    effect: fn(&mut Game, usize),
}

impl PostEffect {
    fn used_up(game: Box<Game>, item_index: usize) -> Self {
        Self {
            game,
            item_index,
            effect: |game, index| {
                game.player.inventory.swap_remove(index);
            },
        }
    }

    fn cancelled(game: Box<Game>, item_index: usize) -> Self {
        Self {
            game,
            item_index,
            effect: |game, _| {
                game.messages.add("Cancelled", WHITE);
            },
        }
    }
}

impl Screen for PostEffect {
    fn update(mut self: Box<Self>, api: &mut dyn DoryenApi) -> Box<dyn Screen> {
        (self.effect)(&mut self.game, self.item_index);
        self.game.update(api)
    }
    fn render(&mut self, api: &mut dyn DoryenApi) {
        self.game.render(api);
    }
}

struct MouseTargeting {
    game: Box<Game>,
    item_index: usize,
    effect: fn((f32, f32), &FoV, &mut Object, &mut [Object], &mut Messages) -> Option<()>,
}

impl MouseTargeting {
    fn confusion(game: Box<Game>, item_index: usize) -> Self {
        Self {
            game,
            item_index,
            effect: confuse,
        }
    }
    fn fireball(game: Box<Game>, item_index: usize) -> Self {
        Self {
            game,
            item_index,
            effect: fireball_explosion,
        }
    }
}

fn target_tile(x: usize, y: usize, fov: &impl FieldOfView) -> Option<Position> {
    if CONSOLE_WIDTH < x || MAP_HEIGHT < y {
        return None;
    }
    if !fov.is_in_fov(x as usize, y as usize) {
        return None;
    }
    Some(Position {
        x: x as u32,
        y: y as u32,
    })
}

fn confuse(
    (x, y): (f32, f32),
    fov: &impl FieldOfView,
    player: &mut Object,
    objects: &mut [Object],
    messages: &mut Messages,
) -> Option<()> {
    let target_pos = target_tile(x as usize, y as usize, fov)?;
    if CONFUSE_RANGE.pow(2) < player.pos.sqr_distance(&target_pos) {
        return None;
    }
    let monster = objects
        .iter_mut()
        .find(|obj| obj.pos == target_pos && obj.ai.is_some())?;
    let old_ai = monster.ai.take().unwrap();
    monster.ai = Some(Ai::Confused {
        previous_ai: Box::new(old_ai),
        num_turns: CONFUSE_NUM_TURNS,
    });
    messages.add(
        format!(
            "The eyes of {} look vacant, as he starts to stumble around!",
            monster.name
        ),
        LIGHT_GREEN,
    );
    Some(())
}

fn fireball_explosion(
    (x, y): (f32, f32),
    fov: &impl FieldOfView,
    player: &mut Object,
    objects: &mut [Object],
    messages: &mut Messages,
) -> Option<()> {
    let target_pos = target_tile(x as usize, y as usize, fov)?;
    messages.add(
        format!(
            "The fireball explodes, burning everything within {} tiles!",
            FIREBALL_RADIUS
        ),
        ORANGE,
    );
    let mut effect = |obj: &mut Object| {
        if obj.pos.sqr_distance(&target_pos) <= FIREBALL_RADIUS.pow(2) && obj.fighter.is_some() {
            messages.add(
                format!(
                    "The {} gets burned for {} hit points.",
                    obj.name, FIREBALL_DAMAGE
                ),
                ORANGE,
            );
            obj.take_damage(FIREBALL_DAMAGE, messages)
        } else {
            None
        }
    };
    effect(player);
    player.fighter.as_mut().unwrap().xp += objects.iter_mut().filter_map(effect).sum::<i32>();
    Some(())
}

impl Screen for MouseTargeting {
    fn update(mut self: Box<Self>, api: &mut dyn DoryenApi) -> Box<dyn Screen> {
        let input = api.input();
        if input.key("Escape") {
            return Box::new(PostEffect::cancelled(self.game, Default::default()));
        }

        if input.mouse_button(0)
            && (self.effect)(
                input.mouse_pos(),
                &self.game.level.fov,
                &mut self.game.player.object,
                &mut self.game.level.objects,
                &mut self.game.messages,
            )
            .is_some()
        {
            return Box::new(PostEffect::used_up(self.game, self.item_index));
        }

        self
    }
    fn render(&mut self, api: &mut dyn DoryenApi) {
        self.game.render(api);
    }
}

struct MainMenu {
    bg: Image,
}

impl MainMenu {
    fn new() -> Self {
        Self {
            bg: Image::new("menu_background.png"),
        }
    }
}

fn load_game() -> Result<SaveState, RonDeError> {
    read_ron(
        &stdweb::web::window()
            .local_storage()
            .get("savegame")
            .unwrap_or_default(),
    )
}

impl Screen for MainMenu {
    fn update(self: Box<Self>, _api: &mut dyn DoryenApi) -> Box<dyn Screen> {
        Box::new(Menu::new(
            "",
            &["Play a new game", "Continue last game"],
            24,
            |menu, choice| match choice {
                Some(0) => Box::new(Game::new()),
                Some(1) => match load_game() {
                    Ok(state) => Box::new(Game::from(state)),
                    Err(_) => {
                        let msg = "No saved game to load.";
                        Box::new(msgbox(msg, msg.len(), menu))
                    }
                },
                _ => menu as Box<dyn Screen>,
            },
        )(self))
    }
    fn render(&mut self, api: &mut dyn DoryenApi) {
        let root = api.con();
        self.bg.blit_2x(root, 0, 0, 0, 0, None, None, None);
        let w = root.get_width() as i32;
        let h = root.get_height() as i32;
        let center_x = w / 2;
        let center_y = h / 2;
        root.print(
            center_x,
            center_y - 4,
            "TOMBS OF THE ANCIENT KINGS",
            TextAlign::Center,
            Some(LIGHT_YELLOW.into()),
            None,
        );
        root.print(
            center_x,
            h - 2,
            "A port of the libtcod tutorial game to doryen-rs.",
            TextAlign::Center,
            Some(LIGHT_YELLOW.into()),
            None,
        );
    }
}

struct MultiScreenEngine {
    state: Option<Box<dyn Screen>>,
}

impl Engine for MultiScreenEngine {
    fn update(&mut self, api: &mut dyn DoryenApi) -> Option<UpdateEvent> {
        self.state = Some(self.state.take()?.update(api));
        None
    }

    fn render(&mut self, api: &mut dyn DoryenApi) {
        if let Some(screen) = &mut self.state {
            screen.render(api);
        }
    }
}

fn main() {
    let window = stdweb::web::window();
    let window_w = window.inner_width();
    let window_h = window.inner_height();
    let w_ratio = window_w as f32 / CONSOLE_WIDTH as f32;
    let h_ratio = window_h as f32 / CONSOLE_HEIGHT as f32;
    let scale_ratio = if w_ratio < h_ratio {
        w_ratio
    } else {
        h_ratio
    };
    let mut app = App::new(AppOptions {
        console_width: CONSOLE_WIDTH as u32,
        console_height: CONSOLE_HEIGHT as u32,
        screen_width: (CONSOLE_WIDTH as f32 * scale_ratio) as u32,
        screen_height: (CONSOLE_HEIGHT as f32 * scale_ratio) as u32,
        window_title: "Doryen tutorial".to_owned(),
        font_path: "terminal_8x8.png".to_owned(),
        max_fps: LIMIT_FPS,
        ..Default::default()
    });
    app.set_engine(Box::new(MultiScreenEngine {
        state: Some(Box::new(MainMenu::new())),
    }));
    app.run();
}
