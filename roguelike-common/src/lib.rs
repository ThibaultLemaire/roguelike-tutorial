pub mod colors;

use colors::*;
use rand::distributions::WeightedIndex;
use rand::prelude::*;
use serde::{Deserialize, Serialize};
use std::cmp;
use std::ops::{Add, Deref, DerefMut, Sub};
pub use vect::vector::Vector;
pub use vect::vector2::Vector2;

const LEVEL_UP_BASE: i32 = 200;
const LEVEL_UP_FACTOR: i32 = 150;

const MONSTER_ATTACK_RANGE: u32 = 2;

const HEAL_AMOUNT: i32 = 40;

const LIGHTNING_DAMAGE: i32 = 40;
const LIGHTNING_RANGE: u32 = 5;

const MAX_ROOM_MONSTERS: [Transition; 3] = [
    Transition { level: 1, value: 2 },
    Transition { level: 4, value: 3 },
    Transition { level: 6, value: 5 },
];
const MAX_ROOM_ITEMS: [Transition; 2] = [
    Transition { level: 1, value: 1 },
    Transition { level: 4, value: 2 },
];
const MONSTER_CHANCES: [(ObjectFactory, &[Transition]); 2] = [
    (
        Object::orc,
        &[Transition {
            level: 1,
            value: 16,
        }],
    ),
    (
        Object::troll,
        &[
            Transition { level: 3, value: 3 },
            Transition { level: 5, value: 6 },
            Transition {
                level: 7,
                value: 12,
            },
        ],
    ),
];
const ITEM_CHANCES: [(ObjectFactory, &[Transition]); 6] = [
    (Object::healing_potion, &[Transition { level: 1, value: 7 }]),
    (
        Object::lightning_bolt_scroll,
        &[Transition { level: 4, value: 5 }],
    ),
    (
        Object::fireball_scroll,
        &[Transition { level: 6, value: 5 }],
    ),
    (
        Object::confusion_scroll,
        &[Transition { level: 2, value: 2 }],
    ),
    (Object::sword, &[Transition { level: 1, value: 1 }]),
    (Object::shield, &[Transition { level: 8, value: 3 }]),
];

pub const CONSOLE_WIDTH: usize = 80;
pub const CONSOLE_HEIGHT: usize = 50;
pub const PANEL_HEIGHT: usize = 7;
pub const MAP_HEIGHT: usize = CONSOLE_HEIGHT - PANEL_HEIGHT;
pub const MSG_HEIGHT: i32 = PANEL_HEIGHT as i32 - 1;

pub const BAR_WIDTH: i32 = 20;
pub const MSG_X: i32 = BAR_WIDTH + 2;

pub const ROOM_MAX_SIZE: u32 = 10;
pub const ROOM_MIN_SIZE: u32 = 6;
pub const MAX_ROOMS: u32 = 30;

pub const LIMIT_FPS: usize = 20;

pub const IDLE: Vector2 = Vector2 { x: 0., y: 0. };
pub const UP: Vector2 = Vector2 { x: 0., y: -1. };
pub const DOWN: Vector2 = Vector2 { x: 0., y: 1. };
pub const LEFT: Vector2 = Vector2 { x: -1., y: 0. };
pub const RIGHT: Vector2 = Vector2 { x: 1., y: 0. };

pub const FOV_LIGHT_WALLS: bool = true;
pub const TORCH_RADIUS: usize = 10;

pub const GREETINGS: &str =
    "Welcome stranger! Prepare to perish in the Tombs of the Ancient Kings.";

pub const INVENTORY_WIDTH: usize = 50;
pub const LEVEL_SCREEN_WIDTH: usize = 40;
pub const CHARACTER_SCREEN_WIDTH: usize = 40;

pub const CONFUSE_NUM_TURNS: u32 = 10;
pub const CONFUSE_RANGE: u32 = 8;

pub const FIREBALL_RADIUS: u32 = 3;
pub const FIREBALL_DAMAGE: i32 = 25;

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub struct Tile {
    pub blocked: bool,
    pub block_sight: bool,
    pub explored: bool,
}
impl Tile {
    pub fn empty() -> Self {
        Tile {
            blocked: false,
            block_sight: false,
            explored: false,
        }
    }

    pub fn wall() -> Self {
        Tile {
            blocked: true,
            block_sight: true,
            explored: false,
        }
    }
}

#[derive(Serialize, Deserialize)]
pub struct Map {
    map: Vec<Vec<Tile>>,
}

impl Map {
    fn new(w: usize, h: usize) -> Self {
        Self {
            map: vec![vec![Tile::wall(); h]; w],
        }
    }

    fn blank(&mut self) {
        let wall = Tile::wall();
        for column in self.map.iter_mut() {
            for tile in column.iter_mut() {
                *tile = wall.clone();
            }
        }
    }

    fn carve_room(&mut self, room: &Rect) {
        for x in (room.x1 + 1)..room.x2 {
            for y in (room.y1 + 1)..room.y2 {
                self.map[x as usize][y as usize] = Tile::empty();
            }
        }
    }

    fn carve_h_tunnel(&mut self, x1: u32, x2: u32, y: u32) {
        for x in cmp::min(x1, x2)..(cmp::max(x1, x2) + 1) {
            self.map[x as usize][y as usize] = Tile::empty();
        }
    }

    fn carve_v_tunnel(&mut self, y1: u32, y2: u32, x: u32) {
        for y in cmp::min(y1, y2)..(cmp::max(y1, y2) + 1) {
            self.map[x as usize][y as usize] = Tile::empty();
        }
    }

    fn width(&self) -> usize {
        self.map.len()
    }

    fn height(&self) -> usize {
        self.map[0].len()
    }
}

impl Deref for Map {
    type Target = Vec<Vec<Tile>>;

    fn deref(&self) -> &Self::Target {
        &self.map
    }
}

impl DerefMut for Map {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.map
    }
}

#[derive(Serialize, Deserialize)]
pub struct Messages {
    messages: Vec<(String, RGB)>,
}
impl Messages {
    pub fn new<T: Into<String>>(first: T, color: RGB) -> Self {
        Self {
            messages: vec![(first.into(), color)],
        }
    }

    pub fn add<T: Into<String>>(&mut self, message: T, color: RGB) {
        self.messages.push((message.into(), color));
    }

    pub fn iter(&self) -> impl DoubleEndedIterator<Item = &(String, RGB)> {
        self.messages.iter()
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Serialize, Deserialize)]
pub enum DeathCallBack {
    Player,
    Monster,
}

#[derive(Clone, Copy, Debug, PartialEq, Serialize, Deserialize)]
pub struct Fighter {
    pub base_max_hp: i32,
    pub hp: i32,
    pub base_defense: i32,
    pub base_power: i32,
    pub xp: i32,
    pub on_death: DeathCallBack,
}

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub enum Ai {
    Basic,
    Confused {
        previous_ai: Box<Ai>,
        num_turns: u32,
    },
}

pub enum UseResult {
    UsedUp,
    UsedAndKept,
    Cancelled,
}

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub enum Item {
    Heal,
    Lightning,
    Confuse,
    Fireball,
    Equipment,
}

#[derive(Clone, Copy, Debug, PartialEq, Serialize, Deserialize)]
pub enum Slot {
    LeftHand,
    RightHand,
    Head,
}

impl std::fmt::Display for Slot {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match *self {
            Slot::LeftHand => write!(f, "left hand"),
            Slot::RightHand => write!(f, "right hand"),
            Slot::Head => write!(f, "head"),
        }
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Equipment {
    pub slot: Slot,
    pub equipped: bool,
    pub power_bonus: i32,
    pub defense_bonus: i32,
    pub max_hp_bonus: i32,
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct Position {
    pub x: u32,
    pub y: u32,
}

impl Position {
    pub fn sqr_distance(&self, other: &Self) -> u32 {
        (self - other).sqr_magnitude() as u32
    }

    fn is_blocked_by_object<'a>(&self, mut objects: impl Iterator<Item = &'a Object>) -> bool {
        objects.any(|object| object.pos == *self && object.blocks)
    }

    fn is_blocked_by_wall(&self, map: &Map) -> bool {
        map[self.x as usize][self.y as usize].blocked
    }

    fn is_blocked<'a>(&self, map: &Map, objects: impl Iterator<Item = &'a Object>) -> bool {
        self.is_blocked_by_wall(map) || self.is_blocked_by_object(objects)
    }
}

impl Into<Vector2> for &Position {
    fn into(self) -> Vector2 {
        Vector2::new(self.x as f64, self.y as f64)
    }
}

impl From<Vector2> for Position {
    fn from(vec: Vector2) -> Self {
        Position {
            x: vec.x.round() as u32,
            y: vec.y.round() as u32,
        }
    }
}

impl Add<Vector2> for &Position {
    type Output = Position;

    fn add(self, rhs: Vector2) -> Self::Output {
        (rhs + self.into()).into()
    }
}

impl Sub<Self> for &Position {
    type Output = Vector2;

    fn sub(self, other: Self) -> Self::Output {
        let self_vec: Vector2 = self.into();
        let other_vec: Vector2 = other.into();
        self_vec - other_vec
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Object {
    pub pos: Position,
    pub level: i32,
    pub char: char,
    pub color: RGB,
    pub name: String,
    pub blocks: bool,
    pub alive: bool,
    pub fighter: Option<Fighter>,
    pub ai: Option<Ai>,
    pub item: Option<Item>,
    pub equipment: Option<Equipment>,
}

#[derive(Serialize, Deserialize)]
pub struct Player {
    pub object: Object,
    pub inventory: Vec<Object>,
}

impl Player {
    fn get_all_equipped(&self) -> Vec<&Equipment> {
        self.inventory
            .iter()
            .filter_map(|item| match &item.equipment {
                Some(e @ Equipment { equipped: true, .. }) => Some(e),
                _ => None,
            })
            .collect()
    }

    pub fn move_or_attack(
        &mut self,
        delta: Vector2,
        objects: &mut [Object],
        map: &Map,
        messages: &mut Messages,
    ) {
        let target_pos = &self.object.pos + delta;
        match objects
            .iter_mut()
            .find(|object| object.pos == target_pos && object.fighter.is_some())
        {
            None => self.object.move_by(delta, map, objects.iter()),
            Some(target) => self.attack(target, messages),
        }
    }

    pub fn is_alive(&self) -> bool {
        self.object.fighter.map_or(false, |f| 0 < f.hp)
    }

    pub fn pick_item_up(&mut self, objects: &mut Vec<Object>, messages: &mut Messages) {
        let index = match objects
            .iter()
            .position(|object| object.pos == self.object.pos && object.item.is_some())
        {
            Some(index) => index,
            None => return,
        };
        if self.inventory.len() >= 26 {
            messages.add(
                format!(
                    "Your inventory is full, cannot pick up {}.",
                    objects[index].name,
                ),
                RED,
            );
            return;
        }
        let mut item = objects.swap_remove(index);
        messages.add(format!("You picked up a {}!", item.name), GREEN);
        if let Some(slot) = item.equipment.as_ref().map(|e| e.slot) {
            if get_equipped_in_slot(slot, &self.inventory).is_none() {
                item.equip(messages);
            }
        }
        self.inventory.push(item);
    }

    pub fn drop_item(
        &mut self,
        item_id: usize,
        messages: &mut Messages,
        objects: &mut Vec<Object>,
    ) {
        let mut item = self.inventory.swap_remove(item_id);
        if let Some(Equipment { equipped: true, .. }) = item.equipment {
            item.dequip(messages);
        }
        item.pos = self.object.pos.clone();
        messages.add(format!("You dropped a {}.", item.name), YELLOW);
        objects.push(item);
    }

    pub fn stats(&self) -> String {
        let Object { level, fighter, .. } = self.object;
        let Fighter {
            xp, base_max_hp, ..
        } = fighter.expect("Player has no fighter struct");
        format!(
            "\
Character information
                
Level: {}
Experience: {}
Experience to level up: {}

Maximum HP: {}
Attack: {}
Defense: {}\
                    ",
            level,
            xp,
            self.object.level_up_xp(),
            base_max_hp,
            self.power().unwrap(),
            self.defense().unwrap(),
        )
    }
}

impl Default for Object {
    fn default() -> Object {
        Object {
            pos: Position { x: 0, y: 0 },
            level: 1,
            char: '?',
            color: WHITE,
            name: "unknown".into(),
            blocks: false,
            alive: false,
            fighter: None,
            ai: None,
            item: None,
            equipment: None,
        }
    }
}

impl Object {
    pub fn player() -> Self {
        Self {
            char: '@',
            name: "player".into(),
            color: WHITE,
            blocks: true,
            alive: true,
            fighter: Some(Fighter {
                base_max_hp: 100,
                hp: 100,
                base_defense: 1,
                base_power: 2,
                xp: 0,
                on_death: DeathCallBack::Player,
            }),
            ..Default::default()
        }
    }

    pub fn stairs() -> Self {
        Self {
            char: '<',
            name: "stairs".into(),
            color: WHITE,
            ..Default::default()
        }
    }

    pub fn orc() -> Self {
        Self {
            char: 'o',
            name: "orc".into(),
            blocks: true,
            alive: true,
            color: DESATURATED_GREEN,
            fighter: Some(Fighter {
                base_max_hp: 20,
                hp: 20,
                base_defense: 0,
                base_power: 4,
                xp: 35,
                on_death: DeathCallBack::Monster,
            }),
            ai: Some(Ai::Basic),
            ..Default::default()
        }
    }

    pub fn troll() -> Self {
        Self {
            char: 'T',
            name: "troll".into(),
            color: DARKER_GREEN,
            fighter: Some(Fighter {
                base_max_hp: 35,
                hp: 35,
                base_defense: 2,
                base_power: 8,
                xp: 100,
                on_death: DeathCallBack::Monster,
            }),
            ..Self::orc()
        }
    }

    pub fn healing_potion() -> Self {
        Self {
            char: '!',
            name: "healing potion".into(),
            color: VIOLET,
            item: Some(Item::Heal),
            ..Default::default()
        }
    }

    pub fn lightning_bolt_scroll() -> Self {
        Self {
            char: '#',
            name: "scroll of lightning bolt".into(),
            color: LIGHT_YELLOW,
            item: Some(Item::Lightning),
            ..Default::default()
        }
    }

    pub fn fireball_scroll() -> Self {
        Self {
            name: "scroll of fireball".into(),
            item: Some(Item::Fireball),
            ..Self::lightning_bolt_scroll()
        }
    }

    pub fn confusion_scroll() -> Self {
        Self {
            name: "scroll of confusion".into(),
            item: Some(Item::Confuse),
            ..Self::lightning_bolt_scroll()
        }
    }

    pub fn sword() -> Self {
        Self {
            char: '/',
            name: "sword".into(),
            color: SKY,
            equipment: Some(Equipment {
                equipped: false,
                slot: Slot::RightHand,
                power_bonus: 3,
                defense_bonus: 0,
                max_hp_bonus: 0,
            }),
            item: Some(Item::Equipment),
            ..Default::default()
        }
    }

    pub fn dagger() -> Self {
        Self {
            char: '-',
            name: "dagger".into(),
            equipment: Some(Equipment {
                equipped: true,
                slot: Slot::LeftHand,
                power_bonus: 2,
                defense_bonus: 0,
                max_hp_bonus: 0,
            }),
            ..Self::sword()
        }
    }

    pub fn shield() -> Self {
        Self {
            char: '[',
            name: "shield".into(),
            color: DARKER_ORANGE,
            equipment: Some(Equipment {
                equipped: false,
                slot: Slot::LeftHand,
                power_bonus: 0,
                defense_bonus: 1,
                max_hp_bonus: 0,
            }),
            ..Self::sword()
        }
    }

    fn equip_dequip(&mut self, value: bool) -> (&String, &Slot) {
        let Equipment { equipped, slot, .. } = self.equipment.as_mut().expect("Not an equipment");
        *equipped = value;
        (&self.name, slot)
    }

    pub fn equip(&mut self, messages: &mut Messages) {
        let (name, slot) = self.equip_dequip(true);
        messages.add(format!("Equipped {} on {}.", name, slot), LIGHT_GREEN);
    }

    pub fn dequip(&mut self, messages: &mut Messages) {
        let (name, slot) = self.equip_dequip(false);
        messages.add(format!("Dequipped {} from {}.", name, slot), LIGHT_YELLOW);
    }

    pub fn move_by<'a>(
        &mut self,
        delta: Vector2,
        map: &Map,
        other_objects: impl Iterator<Item = &'a Object>,
    ) {
        let new_pos = &self.pos + delta;
        if !new_pos.is_blocked(map, other_objects) {
            self.pos = new_pos;
        }
    }

    fn move_towards<'a>(
        &mut self,
        pos: &Position,
        map: &Map,
        other_objects: impl Iterator<Item = &'a Object>,
    ) {
        self.move_by((pos - &self.pos).normalized(), map, other_objects)
    }

    pub fn level_up_xp(&self) -> i32 {
        LEVEL_UP_BASE + self.level * LEVEL_UP_FACTOR
    }

    fn turn_into_corpse(&mut self) {
        self.char = '%';
        self.color = DARK_RED;
        self.name = format!("remains of {}", self.name);
    }
}

pub type ObjectFactory = fn() -> Object;

pub struct Rect {
    pub x1: u32,
    pub y1: u32,
    pub x2: u32,
    pub y2: u32,
}

impl Rect {
    pub fn new(x: u32, y: u32, w: u32, h: u32) -> Self {
        Rect {
            x1: x,
            y1: y,
            x2: x + w,
            y2: y + h,
        }
    }

    pub fn center(&self) -> Position {
        let x = (self.x1 + self.x2) / 2;
        let y = (self.y1 + self.y2) / 2;
        Position { x, y }
    }

    pub fn intersects_with(&self, other: &Rect) -> bool {
        (self.x1 <= other.x2)
            && (self.y1 <= other.y2)
            && (other.x1 <= self.x2)
            && (other.y1 <= self.y2)
    }
}

trait RoguelikeRng: Rng {
    fn fill_room(
        &mut self,
        &Rect { x1, x2, y1, y2 }: &Rect,
        objects: &mut Vec<Object>,
        factory: &impl Fn(&mut Self) -> Object,
        max: u32,
    ) {
        for _ in 0..self.gen_range(0, max + 1) {
            let pos = Position {
                x: self.gen_range(x1 + 1, x2),
                y: self.gen_range(y1 + 1, y2),
            };

            if !pos.is_blocked_by_object(objects.iter()) {
                objects.push(Object {
                    pos,
                    ..factory(self)
                });
            }
        }
    }

    fn gen_room(&mut self, min_size: u32, max_size: u32, map_width: u32, map_height: u32) -> Rect {
        let max_size = max_size + 1;
        let w = self.gen_range(min_size, max_size);
        let h = self.gen_range(min_size, max_size);
        let x = self.gen_range(0, map_width - w);
        let y = self.gen_range(0, map_height - h);
        Rect::new(x, y, w, h)
    }
}

impl RoguelikeRng for ThreadRng {}

pub trait FieldOfView: Sized {
    fn new(width: usize, height: usize, radius: usize, light_walls: bool) -> Self;
    fn default(width: usize, height: usize) -> Self {
        Self::new(width, height, TORCH_RADIUS, FOV_LIGHT_WALLS)
    }
    fn compute_fov(&mut self, pos: &Position);
    fn is_in_fov(&self, x: usize, y: usize) -> bool;
    fn set_transparent(&mut self, x: usize, y: usize, transparent: bool);
    fn init(&mut self, map: &Map) {
        for (x, row) in map.iter().enumerate() {
            for (y, &Tile { block_sight, .. }) in row.iter().enumerate() {
                self.set_transparent(x, y, !block_sight)
            }
        }
    }
}

pub struct Level<T: FieldOfView> {
    level: u32,
    pub map: Map,
    pub objects: Vec<Object>,
    pub fov: T,
}

impl<T: FieldOfView> Level<T> {
    fn from(map: Map, objects: Vec<Object>, level: u32) -> Self {
        Self {
            level,
            objects,
            fov: T::default(map.width(), map.height()),
            map,
        }
    }

    pub fn new(player_pos: &mut Position) -> Self {
        let map_height = MAP_HEIGHT;
        let map_width = CONSOLE_WIDTH;

        let mut new = Self::from(Map::new(map_width, map_height), vec![], 1);
        new.fill(player_pos);
        new
    }

    pub fn take_stairs(&mut self, player: &mut Object, messages: &mut Messages) -> bool {
        let player_on_stairs = self
            .objects
            .iter()
            .any(|object| object.pos == player.pos && object.name == "stairs");
        if !player_on_stairs {
            return false;
        }

        if let Some(Fighter {
            hp, base_max_hp, ..
        }) = player.fighter.as_mut()
        {
            *hp = cmp::max(*hp, *base_max_hp / 2);
        }
        messages.add(
            "After a rare moment of peace, you descend deeper into \
             the heart of the dungeon...",
            RED,
        );
        self.level += 1;
        self.map.blank();
        self.objects.clear();
        self.fill(&mut player.pos);
        return true;
    }

    fn fill(&mut self, player_pos: &mut Position) {
        let room_min_size = ROOM_MIN_SIZE;
        let room_max_size = ROOM_MAX_SIZE;
        let max_rooms = MAX_ROOMS;
        let monster_chances = &MONSTER_CHANCES;
        let item_chances = &ITEM_CHANCES;
        let max_monsters = &MAX_ROOM_MONSTERS;
        let max_items = &MAX_ROOM_ITEMS;

        let map_width = self.map.width() as u32;
        let map_height = self.map.height() as u32;
        let mut rng = thread_rng();
        let mut rooms;
        let mut prev;

        {
            let first_room = rng.gen_room(room_min_size, room_max_size, map_width, map_height);
            self.map.carve_room(&first_room);
            let center = first_room.center();
            *player_pos = center.clone();
            prev = center;
            rooms = vec![first_room];
        }

        let monster_factory = &weighted_choice(monster_chances, self.level);
        let item_factory = &weighted_choice(item_chances, self.level);
        let max_monsters = from_dungeon_level(max_monsters, self.level);
        let max_items = from_dungeon_level(max_items, self.level);
        let objects = &mut self.objects;

        for _ in 1..max_rooms {
            let new_room = rng.gen_room(room_min_size, room_max_size, map_width, map_height);
            let failed = rooms
                .iter()
                .any(|other_room| new_room.intersects_with(other_room));
            if !failed {
                self.map.carve_room(&new_room);
                let new = new_room.center();
                if rng.gen() {
                    self.map.carve_h_tunnel(prev.x, new.x, prev.y);
                    self.map.carve_v_tunnel(prev.y, new.y, new.x);
                } else {
                    self.map.carve_v_tunnel(prev.y, new.y, prev.x);
                    self.map.carve_h_tunnel(prev.x, new.x, new.y);
                }
                prev = new;
                rng.fill_room(&new_room, objects, item_factory, max_items);
                rng.fill_room(&new_room, objects, monster_factory, max_monsters);
                rooms.push(new_room);
            }
        }

        objects.push(Object {
            pos: prev,
            ..Object::stairs()
        });

        self.init_fov(player_pos);
    }

    fn init_fov(&mut self, player_pos: &Position) {
        self.fov.init(&self.map);
        self.fov.compute_fov(player_pos);
    }

    pub fn panel_text(&self) -> String {
        format!("Dungeon level: -{}", self.level)
    }
}

#[derive(Serialize, Deserialize)]
pub struct SaveState {
    player: Player,
    messages: Messages,
    map: Map,
    objects: Vec<Object>,
    level: u32,
}

impl SaveState {
    pub fn new(
        player: Player,
        messages: Messages,
        Level {
            map,
            objects,
            level,
            ..
        }: Level<impl FieldOfView>,
    ) -> Self {
        Self {
            player,
            messages,
            map,
            objects,
            level,
        }
    }

    pub fn open<T: FieldOfView>(self) -> (Player, Messages, Level<T>) {
        let mut level = Level::from(self.map, self.objects, self.level);
        level.init_fov(&self.player.object.pos);
        (self.player, self.messages, level)
    }
}

pub trait Fighting {
    fn name(&self) -> &String;
    fn power(&self) -> Option<i32>;
    fn defense(&self) -> Option<i32>;
    fn gain_xp(&mut self, gain: i32);
    fn take_damage(&mut self, damage: i32, messages: &mut Messages) -> Option<i32>;
    fn try_attack(&mut self, target: &mut impl Fighting, messages: &mut Messages) -> Option<()> {
        let damage = self.power()? - target.defense()?;

        if damage <= 0 {
            None
        } else {
            messages.add(
                format!(
                    "{} attacks {} for {} hit points.",
                    self.name(),
                    target.name(),
                    damage
                ),
                WHITE,
            );
            if let Some(bounty_xp) = target.take_damage(damage, messages) {
                self.gain_xp(bounty_xp);
            }
            Some(())
        }
    }

    fn attack(&mut self, target: &mut impl Fighting, messages: &mut Messages) {
        if let None = self.try_attack(target, messages) {
            messages.add(
                format!(
                    "{} attacks {} but it has no effect!",
                    self.name(),
                    target.name()
                ),
                WHITE,
            )
        }
    }
}

fn player_death(player: &mut Object, messages: &mut Messages) {
    messages.add(format!("You died!"), RED);
    player.turn_into_corpse();
}

fn monster_death(monster: &mut Object, messages: &mut Messages) {
    messages.add(
        format!(
            "{} is dead! You gain {} experience points",
            monster.name,
            monster.fighter.unwrap().xp
        ),
        ORANGE,
    );
    monster.turn_into_corpse();
    monster.blocks = false;
    monster.fighter = None;
    monster.ai = None;
}

impl DeathCallBack {
    fn callback(self, object: &mut Object, messages: &mut Messages) {
        use DeathCallBack::*;
        let callback = match self {
            Player => player_death,
            Monster => monster_death,
        };
        callback(object, messages);
    }
}

impl Fighting for Object {
    fn name(&self) -> &String {
        &self.name
    }
    fn power(&self) -> Option<i32> {
        self.fighter.map(|f| f.base_power)
    }
    fn defense(&self) -> Option<i32> {
        self.fighter.map(|f| f.base_defense)
    }
    fn gain_xp(&mut self, gain: i32) {
        if let Some(Fighter { xp, .. }) = self.fighter.as_mut() {
            *xp += gain;
        }
    }
    fn take_damage(&mut self, damage: i32, messages: &mut Messages) -> Option<i32> {
        if let Some(Fighter {
            hp, xp, on_death, ..
        }) = self.fighter.as_mut()
        {
            if damage > 0 {
                *hp -= damage;
            }
            if *hp <= 0 {
                self.alive = false;
                let xp = *xp;
                on_death.callback(self, messages);
                return Some(xp);
            }
        }
        None
    }
}

impl Fighting for Player {
    fn name(&self) -> &String {
        &self.object.name
    }
    fn power(&self) -> Option<i32> {
        let base_power = self.object.power()?;
        let bonus: i32 = self.get_all_equipped().iter().map(|e| e.power_bonus).sum();
        Some(base_power + bonus)
    }
    fn defense(&self) -> Option<i32> {
        let base_defense = self.object.defense()?;
        let bonus: i32 = self
            .get_all_equipped()
            .iter()
            .map(|e| e.defense_bonus)
            .sum();
        Some(base_defense + bonus)
    }
    fn gain_xp(&mut self, gain: i32) {
        self.object.gain_xp(gain);
    }
    fn take_damage(&mut self, damage: i32, messages: &mut Messages) -> Option<i32> {
        self.object.take_damage(damage, messages)
    }
}

pub fn render_map(
    map: &mut Map,
    fov: &impl FieldOfView,
    mut render: impl FnMut(usize, usize, RGB),
) {
    for (x, row) in map.iter_mut().enumerate() {
        for (
            y,
            Tile {
                ref mut explored,
                block_sight,
                ..
            },
        ) in row.iter_mut().enumerate()
        {
            let visible = fov.is_in_fov(x, y);
            if visible {
                *explored = true;
            }
            if *explored {
                let rgb = match (visible, block_sight) {
                    (false, true) => COLOR_DARK_WALL,
                    (false, false) => COLOR_DARK_GROUND,
                    (true, true) => COLOR_LIGHT_WALL,
                    (true, false) => COLOR_LIGHT_GROUND,
                };
                render(x, y, rgb);
            }
        }
    }
}

pub fn visible_objects<'a>(
    objects: &'a [Object],
    map: &'a Map,
    fov: &'a impl FieldOfView,
) -> impl Iterator<Item = &'a Object> {
    objects.iter().filter(
        move |&Object {
                  pos: Position { x, y },
                  ai,
                  ..
              }| {
            let (x, y) = (*x as usize, *y as usize);
            fov.is_in_fov(x, y) || (map[x][y].explored && ai.is_none())
        },
    )
}

pub fn sort_visible_objects<'a>(
    objects: &'a [Object],
    map: &'a Map,
    fov: &'a impl FieldOfView,
) -> Vec<&'a Object> {
    let mut visible: Vec<_> = visible_objects(objects, map, fov).collect();
    visible.sort_by(|o1, o2| o1.blocks.cmp(&o2.blocks));
    visible
}

fn borrow_mut_at<T>(index: usize, slice: &mut [T]) -> Option<(&mut T, impl Iterator<Item = &T>)> {
    let (head, target_and_tail) = slice.split_at_mut(index);
    let (target, tail) = target_and_tail.split_first_mut()?;
    Some((target, head.iter().chain(tail.iter())))
}

fn ai_basic<'a>(
    monster: &mut Object,
    player: &mut Player,
    messages: &mut Messages,
    fov: &impl FieldOfView,
    map: &Map,
    other_objects: impl Iterator<Item = &'a Object>,
) -> Ai {
    let pos = &monster.pos;
    if fov.is_in_fov(pos.x as usize, pos.y as usize) {
        let target = &player.object.pos;
        if pos.sqr_distance(target) >= MONSTER_ATTACK_RANGE.pow(2) {
            monster.move_towards(target, map, other_objects);
        } else if player.is_alive() {
            monster.attack(player, messages);
        }
    }
    Ai::Basic
}

fn ai_confused<'a>(
    monster: &mut Object,
    previous_ai: Box<Ai>,
    num_turns: u32,
    messages: &mut Messages,
    map: &Map,
    other_objects: impl Iterator<Item = &'a Object>,
) -> Ai {
    match num_turns {
        0 => {
            messages.add(format!("The {} is no longer confused!", monster.name), RED);
            *previous_ai
        }
        n => {
            let mut rng = thread_rng();
            let mut direction =
                Vector2::new(rng.gen_range(-1, 2) as f64, rng.gen_range(-1, 2) as f64);
            direction.normalize();
            monster.move_by(direction, map, other_objects);
            Ai::Confused {
                previous_ai: previous_ai,
                num_turns: n - 1,
            }
        }
    }
}

pub fn ais_take_turn(
    objects: &mut [Object],
    player: &mut Player,
    messages: &mut Messages,
    fov: &impl FieldOfView,
    map: &Map,
) {
    for id in 0..objects.len() {
        if objects[id].ai.is_some() {
            let (monster, rest) = borrow_mut_at(id, objects)
                .expect("we're iterating over the slice so `id` can't be invalid");
            let new_ai = monster.ai.take().map(|ai| match ai {
                Ai::Basic => ai_basic(monster, player, messages, fov, map, rest),
                Ai::Confused {
                    previous_ai,
                    num_turns,
                } => ai_confused(monster, previous_ai, num_turns, messages, map, rest),
            });
            monster.ai = new_ai;
        }
    }
}

pub fn get_equipped_in_slot(target: Slot, inventory: &[Object]) -> Option<usize> {
    inventory
        .iter()
        .enumerate()
        .find_map(|(id, Object { equipment, .. })| match equipment {
            Some(Equipment {
                equipped: true,
                slot,
                ..
            }) if *slot == target => Some(id),
            _ => None,
        })
}

pub fn cast_heal(
    _item_index: usize,
    player: &mut Player,
    messages: &mut Messages,
    _objects: &mut [Object],
    _fov: &impl FieldOfView,
) -> UseResult {
    if let Some(Fighter {
        base_max_hp: max_hp,
        hp,
        ..
    }) = &mut player.object.fighter
    {
        let damage = *max_hp - *hp;
        if damage == 0 {
            messages.add("You are already at full health.", RED);
            return UseResult::Cancelled;
        }
        messages.add("Your wounds start to feel better!", LIGHT_VIOLET);
        *hp += cmp::min(HEAL_AMOUNT, damage);
        return UseResult::UsedUp;
    }
    UseResult::Cancelled
}

fn closest_monster<'a>(
    objects: &'a mut [Object],
    player_pos: &Position,
    fov: &impl FieldOfView,
    max_range: u32,
) -> Option<&'a mut Object> {
    let sqr_max_range = max_range.pow(2);
    objects
        .iter_mut()
        .filter_map(|obj| {
            let pos = &mut obj.pos;
            let sqr_dist = pos.sqr_distance(player_pos);
            if sqr_dist < sqr_max_range
                && obj.ai.is_some()
                && fov.is_in_fov(pos.x as usize, pos.y as usize)
            {
                Some((obj, sqr_dist))
            } else {
                None
            }
        })
        .min_by_key(|(_, sqr_dist)| *sqr_dist)
        .map(|(id, _)| id)
}

pub fn cast_lightning(
    _item_index: usize,
    player: &mut Player,
    messages: &mut Messages,
    objects: &mut [Object],
    fov: &impl FieldOfView,
) -> UseResult {
    let player = &mut player.object;
    match closest_monster(objects, &player.pos, fov, LIGHTNING_RANGE) {
        Some(monster) => {
            messages.add(
                format!(
                    "A lightning bolt strikes the {} with a loud thunder! \
                     The damage is {} hit points.",
                    monster.name, LIGHTNING_DAMAGE
                ),
                LIGHT_BLUE,
            );
            if let Some(xp) = monster.take_damage(LIGHTNING_DAMAGE, messages) {
                player.gain_xp(xp);
            }
            UseResult::UsedUp
        }
        None => {
            messages.add("No enemy is close enough to strike.", RED);
            UseResult::Cancelled
        }
    }
}

pub fn toggle_equipment(
    item_index: usize,
    player: &mut Player,
    messages: &mut Messages,
    _objects: &mut [Object],
    _fov: &impl FieldOfView,
) -> UseResult {
    let &Equipment { equipped, slot, .. } = player.inventory[item_index]
        .equipment
        .as_ref()
        .expect("Not an equipment");
    match equipped {
        false => {
            if let Some(current) = get_equipped_in_slot(slot, &player.inventory) {
                player.inventory[current].dequip(messages);
            }
            player.inventory[item_index].equip(messages)
        }
        true => player.inventory[item_index].dequip(messages),
    }

    UseResult::UsedAndKept
}

struct Transition {
    level: u32,
    value: u32,
}

fn from_dungeon_level(table: &[Transition], level: u32) -> u32 {
    table
        .iter()
        .rev()
        .find(|transition| transition.level <= level)
        .map_or(0, |transition| transition.value)
}

fn weighted_choice<'a>(
    weighted_choices: &'a [(ObjectFactory, &[Transition])],
    level: u32,
) -> impl Fn(&mut ThreadRng) -> Object + 'a {
    let weights = weighted_choices
        .iter()
        .map(|(_, transition_table)| from_dungeon_level(transition_table, level));
    let dist = WeightedIndex::new(weights).unwrap();
    move |rng| weighted_choices[dist.sample(rng)].0()
}
