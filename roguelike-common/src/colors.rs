use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize, Clone, Copy)]
pub struct RGB {
    pub r: u8,
    pub g: u8,
    pub b: u8,
}

impl Into<(u8, u8, u8, u8)> for RGB {
    fn into(self) -> (u8, u8, u8, u8) {
        (self.r, self.g, self.b, 0xFF)
    }
}

pub const BLACK: RGB = RGB {
    r: 0x0,
    g: 0x0,
    b: 0x0,
};
pub const WHITE: RGB = RGB {
    r: 0xFF,
    g: 0xFF,
    b: 0xFF,
};
pub const LIGHT_GREY: RGB = RGB {
    r: 0x9F,
    g: 0x9F,
    b: 0x9F,
};
pub const LIGHT_RED: RGB = RGB {
    r: 0xFF,
    g: 0x3F,
    b: 0x3F,
};
pub const RED: RGB = RGB {
    r: 0xFF,
    g: 0,
    b: 0,
};
pub const DARK_RED: RGB = RGB {
    r: 0xBF,
    g: 0x0,
    b: 0x0,
};
pub const DARKER_RED: RGB = RGB {
    r: 0xBF,
    g: 0x0,
    b: 0x0,
};
pub const ORANGE: RGB = RGB {
    r: 0xFF,
    g: 0x7F,
    b: 0x0,
};
pub const DARKER_ORANGE: RGB = RGB {
    r: 0x7F,
    g: 0x3F,
    b: 0x0,
};
pub const YELLOW: RGB = RGB {
    r: 0xFF,
    g: 0xFF,
    b: 0x0,
};
pub const LIGHT_YELLOW: RGB = RGB {
    r: 0xFF,
    g: 0xFF,
    b: 0x3F,
};
pub const DESATURATED_GREEN: RGB = RGB {
    r: 0x3F,
    g: 0x7F,
    b: 0x3F,
};
pub const LIGHT_GREEN: RGB = RGB {
    r: 0x3F,
    g: 0xFF,
    b: 0x3F,
};
pub const GREEN: RGB = RGB {
    r: 0x0,
    g: 0xFF,
    b: 0x0,
};
pub const DARKER_GREEN: RGB = RGB {
    r: 0x0,
    g: 0x7F,
    b: 0x0,
};
pub const LIGHT_CYAN: RGB = RGB {
    r: 0x3F,
    g: 0xFF,
    b: 0xFF,
};
pub const LIGHT_BLUE: RGB = RGB {
    r: 0x3F,
    g: 0x3F,
    b: 0xFF,
};
pub const SKY: RGB = RGB {
    r: 0x0,
    g: 0xBF,
    b: 0xFF,
};
pub const LIGHT_VIOLET: RGB = RGB {
    r: 0x9F,
    g: 0x3F,
    b: 0xFF,
};
pub const VIOLET: RGB = RGB {
    r: 0x7F,
    g: 0x0,
    b: 0xFF,
};

pub const COLOR_DARK_WALL: RGB = RGB {
    r: 0x0,
    g: 0x0,
    b: 0x64,
};
pub const COLOR_LIGHT_WALL: RGB = RGB {
    r: 0x82,
    g: 0x6E,
    b: 0x32,
};
pub const COLOR_DARK_GROUND: RGB = RGB {
    r: 0x32,
    g: 0x32,
    b: 0x96,
};
pub const COLOR_LIGHT_GROUND: RGB = RGB {
    r: 0xC8,
    g: 0xB4,
    b: 0x32,
};
